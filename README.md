# Branch-Creator

Mass branch Creator using the GitLab API.

## How To Use:

- Add the URL to the file with IDs to the pipeline variables (URL Variable)

- Change the Destiny Branch name (default is newnamedbranch) and Source Branch (default is master) (These can be changed on variables)

- Run the GitLab pipeline

## TODO:

- [ ] Don't run the cloned pipeline
